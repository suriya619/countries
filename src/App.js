import React, { lazy, Suspense } from 'react';
import './App.css';
import Home from './components/Home';
import AutoComplete from './components/AutoComplete';
import Lazydog from './components/Lazydog';
import { BreadCrumbs, BreadCrumbsFirst, BreadCrumbsSecond, BreadCrumbsThird } from './components/BreadCrumbs';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reducers from '../src/components/Redux/Reducer/reduxIndex';
import thunk from "redux-thunk";
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';


const store = createStore(reducers, applyMiddleware(thunk));
// const HomeComp = lazy(() => import('./components/Home'));

function App() {
  return (
    <Provider store={store}>
      {/* <Suspense fallback={<div>Loading...</div>}>
        <Router>
          <BreadCrumbs />
        </Router>
      </Suspense> */}

      <Router>
        <Lazydog />
      </Router>

      {/* <Router>
        <Routes>
          <Route exact path='/' element={<BreadCrumbs />}></Route>
          <Route exact path='/first' element={<BreadCrumbsFirst />}></Route>
          <Route exact path='/second' element={<BreadCrumbsSecond />}></Route>
          <Route exact path='/third' element={<BreadCrumbsThird />}></Route>
        </Routes>
      </Router> */}
    </Provider>
  )
}

export default App;
