import React, { useState, useEffect } from "react";
import { useDispatch, connect } from "react-redux";
import { selected_Countries, selected_Countries_CheckBox, getCountries } from "./Redux/Action/homeAction";
import Box from '@mui/material/Box';
import List from '@mui/material/List';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Checkbox from '@mui/material/Checkbox';
import ListItemButton from '@mui/material/ListItemButton';
import Collapse from '@mui/material/Collapse';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import StarBorder from '@mui/icons-material/StarBorder';
import Accordion from '@mui/material/Accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import AccordionSummary from '@mui/material/AccordionSummary';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Avatar from '@mui/material/Avatar';
import Chips from "./Chips";
import Pagination from '@mui/material/Pagination';





const Home = (props) => {

    const { listCountries, selectedContries, chipsCountry, checkState } = props;
    const dispatchData = useDispatch();
    const [open, setOpen] = React.useState(false);
    const [inneropen, setInnerOpen] = React.useState(false);
    const [Idx, setIdx] = useState(null)
    const [expanded, setExpanded] = React.useState(false);
    const [lang, setLang] = React.useState(false);
    const [arr, setArr] = React.useState([]);
    const [filteredList, setFilteredList] = React.useState([]);
    const pageSize = 10;
    const [pagination, setPagination] = React.useState({
        count: 0,
        from: 0,
        to: pageSize
    });


    useEffect(() => {
        dispatchData(getCountries());
    }, [])

    useEffect(() => {
        chipsCountry(arr)
    }, [arr])

    useEffect(() => {
        setFilteredList(listCountries?.sort((a, b) => ((a.name?.common).toLowerCase() > (b.name?.common).toLowerCase()) ? 1 : -1).slice(0, 20))
    }, [listCountries])

    useEffect(() => {
        setPagination({ ...pagination, count: listCountries?.length })
    }, [pagination.from, pagination.to])


    const handleAccordion = (idx) => {
        setIdx(idx)
        setExpanded(true)
    };
    const nameClick = (index) => {
        setInnerOpen(index)
        setOpen(!open)
    };
    const languageClick = (idx) => {
        setInnerOpen(idx)
        setLang(!lang)
    };
    const checkboxChange = (chk) => {
        checkState(chk)

        const pushpop = arr.find((e) => e === chk)

        if (!pushpop) {
            setArr([...arr, chk])
        }
        else {
            const chkBox = arr.filter((e) => e !== chk)
            setArr(chkBox)
        }
    }

    const handlePagechange = (event) => {
        const page = parseInt(event?.target?.textContent)
        const from = (page - 1) * pageSize
        const to = (page - 1) * pageSize + pageSize
        setPagination({ ...pagination, from: from, to: to })
        console.log(listCountries.slice(from, to));
        console.log(from, 'from', to, 'to');
    }


    return (
        <>
            <Chips />
            <Box component="div" sx={{ overflow: 'auto' }}>
                {(filteredList || [])?.sort((a, b) => (
                    (a.name?.common).toLowerCase() > (b.name?.common).toLowerCase()) ? 1 : -1)?.slice(pagination.from, pagination.to)?.map((value, index) => {
                        return (
                            <Accordion sx={{ width: '100%' }} key={index} onChange={() => handleAccordion(index)} expanded={expanded === true && index === Idx}>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}>
                                    <Avatar sx={{ width: 24, height: 24, mr: '10px' }} src={value?.flags?.png} />
                                    <Typography sx={{ width: '33%', flexShrink: 0 }}>
                                        {value?.name?.common}
                                    </Typography>
                                    <Typography sx={(selectedContries.indexOf(value?.name?.common) > -1) ? { color: 'green' } : { color: 'text.secondary' }}>{selectedContries.indexOf(value?.name?.common) > -1 ? "Selected" : "Not Selected"}</Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <List key={index} >
                                        <ListItemButton>
                                            <ListItemText primary={'Select'} />
                                            <Checkbox onChange={() => checkboxChange(value?.name?.common)} checked={selectedContries.indexOf(value?.name?.common) > -1 ? true : false} />
                                        </ListItemButton>
                                        <ListItemButton >
                                            <ListItemText primary={value?.region} />
                                        </ListItemButton>
                                        <ListItemButton onClick={() => nameClick(index)}>
                                            <ListItemText primary={value?.name?.official} />
                                            {open && index === inneropen ? <ExpandLess /> : <ExpandMore />}
                                        </ListItemButton>
                                        <Collapse in={open && index === inneropen} timeout="auto" unmountOnExit >
                                            <List component="div" disablePadding>
                                                <ListItemButton sx={{ pl: 4 }}>
                                                    <ListItemIcon>
                                                        <StarBorder />
                                                    </ListItemIcon>
                                                    <ListItemText primary={'Capital : ' + value?.capital} />
                                                </ListItemButton>
                                                <ListItemButton sx={{ pl: 4 }}>
                                                    <ListItemIcon>
                                                        <StarBorder />
                                                    </ListItemIcon>
                                                    <ListItemText primary={'Continents : ' + value?.continents} />
                                                </ListItemButton>
                                            </List>
                                        </Collapse>

                                        <ListItemButton onClick={() => languageClick(index)}>
                                            <ListItemText primary='Languages' />
                                            {lang && index === inneropen ? <ExpandLess /> : <ExpandMore />}
                                        </ListItemButton>
                                        <Collapse in={lang && index === inneropen} timeout="auto" unmountOnExit >
                                            {(value?.languages !== undefined) ?
                                                <List component="div" disablePadding>
                                                    {Object.keys(value?.languages || {}).map((x, i) => {
                                                        return (
                                                            <ListItemButton key={i}>
                                                                <ListItemIcon>
                                                                    <StarBorder />
                                                                </ListItemIcon>
                                                                <ListItemText primary={(value?.languages || {})[x]} />
                                                            </ListItemButton>
                                                        )
                                                    })}
                                                </List> :
                                                <List component="div" disablePadding>
                                                    <ListItemButton >
                                                        <ListItemText primary={'No languages found'} />
                                                    </ListItemButton>
                                                </List>
                                            }
                                        </Collapse>
                                    </List>
                                </AccordionDetails>
                            </Accordion>
                        )
                    })}
                <Pagination count={filteredList.length} onChange={(e) => handlePagechange(e)} />
            </Box>
        </>
    )
};


const mapStateToProps = (state) => {
    const { listCountries, selectedContries } = state.auth;
    return {
        listCountries,
        selectedContries,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        chipsCountry: (arr) => { return dispatch(selected_Countries(arr)) },
        checkState: (chk) => { return dispatch(selected_Countries_CheckBox(chk)) },
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Home);