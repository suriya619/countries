import React, { useState, useEffect } from "react";
import { useDispatch, connect } from "react-redux";
import { removed_Countries_Chips, selected_Countries } from "./Redux/Action/homeAction";
import Chip from '@mui/material/Chip';
import Stack from '@mui/material/Stack';
import Avatar from '@mui/material/Avatar';


const Chips = (props) => {

    const { selectedContries, removedCountries, chipsCountry, listCountries } = props;
    const [flag, setFlag] = useState([]);


    useEffect(() => {
        flagIcons();
    }, [selectedContries])


    const handleDelete = (chip) => {
        const result = (selectedContries || []).filter(filtered => filtered !== chip);
        chipsCountry(result)
    }

    const flagIcons = () => {
        const filterFlag = (selectedContries.map((x) => {
            return {
                demoArr: (listCountries.filter((y) => (x === y?.name?.common)))
            }
        })).flat()
        setFlag(filterFlag)
    }


    return (
        <>
            <Stack direction="row" spacing={1}>
                {flag.map((chip, index) => {
                    return (
                        <Chip label={chip?.demoArr[0]?.name?.common} key={index} variant="outlined"
                            onDelete={() => handleDelete(chip?.demoArr[0]?.name?.common)}
                            avatar={<Avatar alt={chip?.demoArr[0]?.name?.common[0].toUpperCase()}
                                src={(chip?.demoArr[0]?.flags?.svg) ? (chip?.demoArr[0]?.flags?.svg) : (chip?.demoArr[0]?.flags?.png)} />} />
                    )
                })}
            </Stack>
        </>
    )
}

const mapStateToProps = (state) => {
    const { selectedContries, listCountries } = state.auth;
    return {
        selectedContries,
        listCountries,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        chipsCountry: (arr) => { return dispatch(selected_Countries(arr)) },
        removedCountries: (rem) => { return dispatch(removed_Countries_Chips(rem)) },
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Chips);