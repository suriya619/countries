import { GET_COUNTRIES, SELECTED_CONTRIES, SELECTED_CONTRIES_CHECKBOX, REMOVED_CHIPS_COUNTRIES, LAZYONE_TOPARENT } from "../reduxIndex";


export const capture_getCountries = (result) => {
    return {
        type: GET_COUNTRIES,
        payload: result
    }
};

export const getCountries = () => {
    return async (dispatch) => {
        await fetch(`https://restcountries.com/v3.1/all`)
            .then(response => response.json())
            .then(response => {
                dispatch(capture_getCountries(response))
            })
    }
};


export const selected_Countries = (result) => {
    return {
        type: SELECTED_CONTRIES,
        payload: result
    }
};

export const selected_Countries_CheckBox = (result) => {
    return {
        type: SELECTED_CONTRIES_CHECKBOX,
        payload: result
    }
};

export const removed_Countries_Chips = (result) => {
    return {
        type: REMOVED_CHIPS_COUNTRIES,
        payload: result
    }
};

export const sent_LazyOne_ToParent = (result) => {
    console.log(result, 'action')
    return {
        type: LAZYONE_TOPARENT,
        payload: result
    }
};