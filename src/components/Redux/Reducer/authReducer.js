import { GET_COUNTRIES, SELECTED_CONTRIES, REMOVED_CHIPS_COUNTRIES, LAZYONE_TOPARENT } from "../reduxIndex";

const initialState = {
    listCountries: [],
    selectedContries: [],
    removedChips: [],
    lazyone: {},
}

// const removed_Contries_Checkbox = (data, sel) => {

//     const result = (data || []).filter(filtered => filtered !== sel);
//     return (
//         result
//     )
// }


const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_COUNTRIES:
            return {
                ...state,
                listCountries: action.payload
            }
        case SELECTED_CONTRIES:
            return {
                ...state,
                selectedContries: action.payload
            }
        // case REMOVED_CHIPS_COUNTRIES:
        //     return {
        //         ...state,
        //         removedChips: removed_Contries_Checkbox(state.selectedContries, action.payload)
        //     }
        case LAZYONE_TOPARENT:
            console.log(action.payload, 'reducer')
            return {
                ...state,
                lazyone: action.payload
            }
        default:
            return state;
    }
}

export default authReducer;