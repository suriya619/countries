import React, { useState, useEffect } from "react";
import { useDispatch, connect } from "react-redux";
import { getCountries, sent_LazyOne_ToParent } from "./Redux/Action/homeAction";
import {
    Autocomplete, TextField, Box, Typography, CircularProgress, List, Button, Breadcrumbs, Link, Paper, Grid, Card,
    CardActions, CardContent, CardMedia, LinearProgress, Skeleton, CardHeader, Avatar, Backdrop,
} from "@mui/material";
import { ConnectingAirportsOutlined, Padding } from "@mui/icons-material";
import { styled, alpha } from '@mui/material/styles';




const Lazydog = (props) => {

    const dispatchData = useDispatch();
    const { listCountries, getDispatchCountries, } = props;
    const [progress, setProgress] = React.useState(0);
    const [loader, setLoader] = React.useState(true);
    const [loaderHunderd, setLoaderHunderd] = React.useState(0);
    const [skeletonLoad, setSkeletonLoad] = React.useState(true);
    const [value, setValue] = useState({});
    const [backdropMap, setBackdropMap] = React.useState(false);

    const handleCloseBachdrop = () => {
        setBackdropMap(false)
    }

    console.log(backdropMap, 'backdropMap')
    useEffect(() => {
        getDispatchCountries().then(() => {
            setLoader(false)
            setSkeletonLoad(false)
        })
    }, [!loader])

    useEffect(() => {
        const timer = setInterval(() => {
            setProgress((oldProgress) => {
                if (oldProgress === 100) {
                    setLoaderHunderd(100)
                    return 0;
                }
                const diff = Math.random() * 10;
                return Math.min(oldProgress + diff, 100);
            });
        }, 500);

        return () => {
            clearInterval(timer);
        };
    })

    return (
        <React.Fragment >
            <Paper >
                {backdropMap ?
                    <Backdrop
                        sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                        open={backdropMap} onClick={handleCloseBachdrop} >
                        <Card>
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="div">
                                    Countries
                                </Typography>
                                <Typography variant="body2" color="text.secondary">
                                    Lizards are a widespread group of squamate reptiles, with over 6,000
                                    species, ranging across all continents except Antarctica
                                </Typography>
                            </CardContent>

                        </Card>

                    </Backdrop> :
                    <Grid container spacing={3}>
                        <Grid item xs={12} >
                            <Lazyone listCountries={listCountries} setValue={setValue} />
                        </Grid>
                        <Grid item xs={12} sm={6} md={6} lg={6} xl={6}>
                            <Lazytwo listCountries={listCountries} skeletonLoad={skeletonLoad} value={value} />
                        </Grid>
                        <Grid item xs={12} sm={6} md={6} lg={6} xl={6}>
                            <Lazythree listCountries={listCountries} progress={progress} loader={loader} loaderHunderd={loaderHunderd} value={value} setBackdropMap={setBackdropMap} />
                        </Grid>
                    </Grid>}
            </Paper>
        </React.Fragment>
    )
}

export const Lazyone = (props) => {

    const { listCountries, setValue } = props;

    const [inputValue, setInputValue] = React.useState('');
    const [open, setOpen] = React.useState(false);
    const [options, setOptions] = React.useState([]);
    const [loading, setLoading] = React.useState(false);


    return (
        <React.Fragment>
            <Card >
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        Countries
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        Lizards are a widespread group of squamate reptiles, with over 6,000
                        species, ranging across all continents except Antarctica
                    </Typography>
                </CardContent>

                <CardContent>
                    <Autocomplete
                        onChange={(event, newValue) => {
                            setValue(newValue);
                        }}
                        onInputChange={(event, newInputValue) => {
                            setInputValue(newInputValue);
                        }}
                        onOpen={() => {
                            setOpen(true);
                        }}
                        onClose={() => {
                            setOpen(false);
                        }}
                        open={open}
                        id="controllable-states-demo"
                        freeSolo
                        loading={loading}
                        inputValue={inputValue}
                        options={listCountries?.sort((a, b) => ((a.name?.common).toLowerCase() > (b.name?.common).toLowerCase()) ? 1 : -1) || []}
                        getOptionLabel={(option) => option?.name?.common}
                        renderInput={(params) => <TextField {...params}
                            label="List of Countries" />}
                        renderOption={(props, option) => {
                            return (
                                <Box {...props} key={option?.name?.common}>
                                    <Avatar sx={{ width: 24, height: 24, mr: '10px' }}
                                        alt={option?.name?.common[0].toUpperCase()}
                                        src={(option?.flags?.svg) ? (option?.flags?.svg) : (option?.flags?.png)} />
                                    <Typography>{option?.name?.common}</Typography>
                                </Box>
                            );
                        }}>
                    </Autocomplete>
                </CardContent>
            </Card>
        </React.Fragment>
    )
}

export const Lazytwo = (props) => {
    const { listCountries, skeletonLoad, value } = props;

    return (
        <React.Fragment>

            <Card >
                <CardContent sx={{ pb: '1px' }}>
                    {skeletonLoad ? <Skeleton variant="rounded" width={200} height={25} animation="wave" /> :
                        <Typography gutterBottom variant="h5" component="div">
                            {(value?.name?.common) ? (value?.name?.common) : ''}
                        </Typography>}
                </CardContent>
                <CardContent>
                    <Grid container spacing={2}>
                        <Grid item >
                            <Typography variant="subtitle1" color="text.secondary" component="div">
                                Capital:
                            </Typography>
                        </Grid>
                        <Grid item >
                            <Typography variant="subtitle1"> {(value?.capital) ? (value?.capital[0]) : ''}</Typography>
                        </Grid>
                    </Grid>
                </CardContent>
                <CardContent>
                    <Grid container spacing={2}>
                        <Grid item >
                            <Typography variant="subtitle1" color="text.secondary" component="div">
                                Continent:
                            </Typography>
                        </Grid>
                        <Grid item >
                            <Typography variant="subtitle1"> {(value?.continents) ? (value?.continents[0]) : ''}</Typography>
                        </Grid>
                    </Grid>
                </CardContent>
                <CardContent>
                    <Grid container spacing={2}>
                        <Grid item >
                            <Typography variant="subtitle1" color="text.secondary" component="div">
                                Status:
                            </Typography>
                        </Grid>
                        <Grid item >
                            <Typography variant="subtitle1"> {(value?.status) ? (value?.status) : ''}</Typography>
                        </Grid>
                    </Grid>
                </CardContent>
                <CardContent>
                    <Grid container spacing={2}>
                        <Grid item >
                            <Typography variant="subtitle1" color="text.secondary" component="div">
                                Population:
                            </Typography>
                        </Grid>
                        <Grid item >
                            <Typography variant="subtitle1"> {(value?.population) ? (value?.population) : ''}</Typography>
                        </Grid>
                    </Grid>
                </CardContent>
                <CardContent>
                    <Grid container spacing={2}>
                        <Grid item >
                            <Typography variant="subtitle1" color="text.secondary" component="div">
                                UN Member:
                            </Typography>
                        </Grid>
                        <Grid item >
                            <Typography variant="subtitle1"> {(value?.unMember) ? 'Yes' : 'No'}</Typography>
                        </Grid>
                    </Grid>
                </CardContent>
            </Card>

        </React.Fragment>
    )
}

export const Lazythree = (props) => {
    const { progress, loader, loaderHunderd, value, setBackdropMap } = props;

    return (
        <React.Fragment>
            <Card >
                {loader ? <LinearProgress variant="determinate" value={(loaderHunderd === 100) ? (100) : (progress)} /> : null}
                <CardMedia
                    component="img"
                    alt="green iguana"
                    height="140"
                    image="/static/images/cards/contemplative-reptile.jpg"
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        {(value?.capital) ? (value?.capital) : ''}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        Lizards are a widespread group of squamate reptiles, with over 6,000
                        species, ranging across all continents except Antarctica ead group of squamate reptiles, with over 6,000
                        species, ranging across all continents except Antarcticaead group of squamate reptiles, with over 6,000
                        species, ranging across all continents except Antarctica
                    </Typography>
                </CardContent>
                {((Object.keys((value == undefined || null) ? {} : value).length === 0)) ?
                    null :
                    < CardActions >
                        <Button size="small" onClick={() => setBackdropMap(true)}>View MAp</Button>
                    </CardActions>
                }
            </Card>
        </React.Fragment >
    )
}


const mapStateToProps = (state) => {
    const { listCountries } = state.auth;
    return {
        listCountries,
    }
}
const mapDispatchToProps = (dispatch) => {

    return {
        getDispatchCountries: () => { return dispatch(getCountries()) },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Lazydog);