import React, { useState, useEffect } from "react";
import { useDispatch, connect } from "react-redux";
import { Autocomplete, TextField, Box, Typography, CircularProgress, List } from "@mui/material";
import { getCountries } from "./Redux/Action/homeAction";
import Avatar from '@mui/material/Avatar';



const AutoComplete = (props) => {
    const { listCountries } = props;
    const dispatchData = useDispatch();
    const [value, setValue] = useState('');
    const [inputValue, setInputValue] = React.useState('');
    const [open, setOpen] = React.useState(false);
    const [options, setOptions] = React.useState([]);
    const [loading, setLoading] = React.useState(false);
    const [opt, setOpt] = useState([]);

    console.log(listCountries)
    useEffect(() => {
        dispatchData(getCountries());
    }, [])

    useEffect(() => {
        if (open) {
            if (listCountries && listCountries.length === 0) {
                setLoading(true)
            }
            else {
                setLoading(false)
            }
        }
        else {
            setLoading(false)
        }
    })


    return (
        <>
            <Box>
                <Typography></Typography>
                <Autocomplete
                    onChange={(event, newValue) => {
                        setValue(newValue);
                    }}
                    onInputChange={(event, newInputValue) => {
                        setInputValue(newInputValue);
                    }}
                    onOpen={() => {
                        setOpen(true);
                    }}
                    onClose={() => {
                        setOpen(false);
                    }}
                    multiple
                    disableCloseOnSelect
                    open={open}
                    id="controllable-states-demo"
                    freeSolo
                    loading={loading}
                    inputValue={inputValue}
                    options={listCountries.sort((a, b) => ((a.continents[0]).toLowerCase() > (b.continents[0]).toLowerCase()) ? 1 : -1) || []}
                    getOptionLabel={(option) => option?.name?.common}
                    groupBy={(option) => option?.continents[0]}
                    renderInput={(params) => <TextField {...params}
                        label="List of Countries"
                        InputProps={{
                            ...params.InputProps,
                            endAdornment: (
                                <React.Fragment>
                                    {loading ? <CircularProgress color="inherit" size={20} /> : null}
                                    {params.InputProps.endAdornment}
                                </React.Fragment>
                            ),
                        }} />}
                    renderOption={(props, option) => {
                        return (
                            <Box {...props} key={option?.name?.common}>
                                <Avatar sx={{ width: 24, height: 24, mr: '10px' }}
                                    alt={option?.name?.common[0].toUpperCase()}
                                    src={(option?.flags?.svg) ? (option?.flags?.svg) : (option?.flags?.png)} />
                                <Typography>{option?.name?.common}</Typography>
                            </Box>
                        );
                    }}
                    renderGroup={(params) => {
                        console.log(params)
                        return (
                            <List key={params.key}>
                                <Typography variant="h5">{params.group}</Typography>
                                <Typography >{params.children}</Typography>
                            </List>
                        )
                    }}>
                </Autocomplete>
            </Box>

        </>
    )
}

const mapStateToProps = (state) => {
    const { listCountries } = state.auth;
    return {
        listCountries,
    }
}

export default connect(mapStateToProps, null)(AutoComplete);