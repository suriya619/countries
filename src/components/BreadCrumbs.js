import React, { useState, useEffect } from "react";
import { useDispatch, connect } from "react-redux";
import { Autocomplete, TextField, Box, Typography, CircularProgress, List, Button, Breadcrumbs, Link } from "@mui/material";


export const BreadCrumbs = () => {
    const forwardFirst = () => {
    }
    return (
        <React.Fragment>
            <Breadcrumbs aria-label="breadcrumb">
                <Link underline="hover" color="inherit" href="/">
                    HOME
                </Link>
                <Link underline="hover" color="inherit" href="/first" >
                    first
                </Link>
                <Link underline="hover" color="inherit" href="/second" >
                    second
                </Link>
                <Link underline="hover" color="inherit" href="/third" >
                    third
                </Link>
                <Typography color="text.primary">Breadcrumbs</Typography>
            </Breadcrumbs>
        </React.Fragment>


    )
}

export const BreadCrumbsFirst = () => {
    return (
        <React.Fragment>
            <BreadCrumbs />
            <Button variant="outlined">First</Button>
        </React.Fragment>
    )
}

export const BreadCrumbsSecond = () => {
    return (

        <React.Fragment>
            <BreadCrumbs />
            <Button variant="outlined">Second</Button>
        </React.Fragment>
    )
}

export const BreadCrumbsThird = () => {
    return (
        <React.Fragment>
            <BreadCrumbs />
            <Button variant="outlined">Third</Button>
        </React.Fragment>
    )
}

